import { MysqlConnectionOptions }  from "typeorm/driver/mysql/MysqlConnectionOptions";

const config: MysqlConnectionOptions = {
  "type": "mysql",
  "host": "demo.org",
  "port": 3306,
  "username": "root",
  "password": "123456",
  "database": "nestjs_node",
  "entities": ["dist/**/*.entity{.ts,.js}"],
  "synchronize": true
};

export default config;