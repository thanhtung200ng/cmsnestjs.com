import { Module } from '@nestjs/common';

//database
import {TypeOrmModule} from "@nestjs/typeorm";
import configDatabase from '../config/database/ormconfig';

//object
import { CustomerModule } from './customer/customer.module';
import {UsersModule} from "./users/users.module";

@Module({
  imports: [
      TypeOrmModule.forRoot(configDatabase),
      CustomerModule,
      UsersModule
  ],
  controllers: [],
})
export class AppModule {}
