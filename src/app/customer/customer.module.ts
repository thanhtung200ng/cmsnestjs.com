import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CustomerEntity } from './customer.entity';
import { CustomerController } from './customer.controller'

@Module({
    imports: [TypeOrmModule.forFeature([CustomerEntity])],
    providers: [],
    exports: [],
    controllers: [CustomerController],
})
export class CustomerModule {}
