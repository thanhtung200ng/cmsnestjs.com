import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class CustomerEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    first_name: string;

    @Column()
    last_name: string;
    // @Column();
    @Column()
    phone: number;

    @Column()
    email: string;

}